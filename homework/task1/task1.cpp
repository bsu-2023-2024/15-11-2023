#include <iostream>

using namespace std;

void randomArray(int (& arr)[10], int);
void displayArray(int[], int);
int negativeEvenSum(int[], int);
bool isPrime(int);
int positivePrime(int[], int);
int prime(int[], int);


int main()
{

	const int n = 10;
	int set[n]{ 0 };
	randomArray(set, n);
	displayArray(set, n);	
	cout << prime(set, n) << endl;
	displayArray(set, n);
}

void displayArray(int numbers[], int n)
{
	for (size_t i = 0; i < n; i++)
	{
		cout << numbers[i] << " | ";
	}
	cout << endl;
}

void randomArray(int (&numbers)[10], int n)
{
	srand(time(NULL));
	for (int i = 0; i < n; i++)
	{
		numbers[i] = -500 + rand() % 1000;
	}
}	

int negativeEvenSum(int set[10], int n)
{
	int sum = 0;
	for (size_t i = 1; i < n; i+=2)
	{
		if (set[i] < 0)
		{
			sum += set[i];
		}
	}
	return sum;
}

bool isPrime(int n)
{
	if (n == 1)
	{
		return false;
	}
	for (size_t i = 2; i < n; i++)
	{
		if (n % i == 0)
		{
			return false;
		}
	}
	return true;
}

int positivePrime(int set[10], int n)
{
	int sum = 0;
	for (size_t i = 1; i < n; i++)
	{
		if (isPrime(i+1) && set[i] > 0)
		{
			sum += set[i];
		}
	}
	return sum;
}

int prime(int set[], int n)
{
	int sum = 0;
	for (size_t i = 1; i < n; i++)
	{
		if (isPrime(set[i]))
		{
			sum += set[i];
		}
	}
	return sum;
}