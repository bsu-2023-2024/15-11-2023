#include <iostream>

using namespace std;

bool isPalindrome(int, int);
int getLength(int);

int main()
{
	int n = 0;
	cout << "Enter the number to check if it's a palindrome: ";
	cin >> n;

	if (isPalindrome(n, pow(10, getLength(n)-1)))
	{
		cout << "Number is palindrome";
	}
	else
	{
		cout << "Number is not palindrome";
	}

}

int getLength(int n)
{
	int i = 10, length = 0;
	while (n % i)
	{
		n /= 10;
		length++;
	}
	return length;
}

bool isPalindrome(int n, int length)
{
	if (n > 10)
	{
		if (n / length == n % 10)
		{
			return isPalindrome((n % length) / 10, length / 100);
		}
		else
		{
			return false;
		}
	}
	return true;
}