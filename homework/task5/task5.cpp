#include <iostream>

using namespace std;

void randomArray(int(&arr)[1000], int);
void displayArray(int[], int);
int maxElement(int[], int firstIndex, int secondIndex, int& maxIndex, int length);

int main()
{
	const int m = 1000;
	int set[m]{ 0 };
	randomArray(set, m);
	displayArray(set, m);
	int max = 0;
	cout << maxElement(set, 0, 1, max, m);
}


void displayArray(int numbers[], int n)
{
	for (size_t i = 0; i < n; i++)
	{
		cout << numbers[i] << " | ";
	}
	cout << endl;
}

void randomArray(int(&numbers)[1000], int n)
{
	srand(time(NULL));
	for (int i = 0; i < n; i++)
	{
		numbers[i] = rand();
	}
}

int maxElement(int set[], int firstIndex, int secondIndex, int& maxIndex, int length)
{
	if (firstIndex < length) 
	// I wrote first but not second as in 44 string I compare first element earlier than second
	// and 48 string will be executed only if 44 isn't true. Thus in odd-elements array the last element will be compared
	{
		if (set[maxIndex] < set[firstIndex])
		{
			maxIndex = firstIndex;
		}
		else if (set[maxIndex] < set[secondIndex])
		{
			maxIndex = secondIndex;
		}
		return maxElement(set, firstIndex + 2, secondIndex + 2, maxIndex, length);
	}
	else
	{
		return set[maxIndex];
	}
}